# desafio_allya

Esse projeto está sendo utilizado para um processo seletivo e tem como objetivo realizar uma automação para o ambiente de testes da Allya. 

O projeto consiste em validar um fluxo natural que o usuário faria ao "navegar" pelo site. 

Foi utilizado selenium + cucumber. 

Requisitos 
• Java 8+

• Maven 4.0.0 

• IntelliJ - IDE utilizada. 

Branches

• Master - Contem o projeto final com a versão mais estável. 

• Developer - branch de desenvolvimento.

• Core_Project - coração do projeto contém todas as alterações feitas.