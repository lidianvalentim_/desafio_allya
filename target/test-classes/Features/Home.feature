#language: pt
Funcionalidade: Validar o fluxo de um usuário

  Como usuário gostaria de navegar pelo site da allya.

  Contexto:
    Dado que eu estou na página inicial do site da allya.

  @FluxoBasico @BotoesCabecalho
  Cenário: Validar se os botoes do cabeçalho redirecionam o usuario a sua secao.
    Quando clico no botao Sobre
    Então sou redirecionada a secao
    Quando clico no botao Contato
    Entao sou redirecionada a secao

  @ClicarnoBotaoLeiaMais @Sobre
  Cenário: Validar o redirecionamento a uma nova pagina.
    Quando clico no botao Sobre
    Então sou redirecionada a secao
    Quando clico no botao Leia Mais
    Então abre uma nova janela






