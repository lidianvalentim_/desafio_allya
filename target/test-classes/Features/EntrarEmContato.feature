#language: pt

Funcionalidade: Validar o envio de uma mensagem através do canal Entre em contato conosco.


  @EnviarMensagemSucesso @EntreEmContatoConosco
  Esquema do Cenário: Enviar uma mensagem atraves do canal Entre em Contato Conosco
    Quando clico no botao Contato
    E Preencho o campo "<Nome>", "<Email>", "<Telefone>" e "<mensagem>"
    Quando clico em enviar
    Entao a mensagem é enviada com sucesso

    Exemplos:
      | Nome             | Email             | Telefone    | mensagem                                                                                        |
      | Lidiane Valentim | lidiane@teste.com | 81996077788 | Olá Gostaria de receber mais informações para adicionar minha empresa ao programa de benefícios |

  @EnviarMensagemErro @EntreEmContatoConosco
  Esquema do Cenário: Enviar uma mensagem atraves do canal Entre em Contato Conosco com erro no email
    Quando clico no botao Contato
    E Preencho o campo "<Nome>", "<Email>", "<Telefone>" e "<mensagem>"
    Quando clico em enviar
    Entao o campo email é exibido com erro

    Exemplos:
      | Nome             | Email | Telefone    | mensagem                                                                                        |
      | Lidiane Valentim |       | 81996077788 | Olá Gostaria de receber mais informações para adicionar minha empresa ao programa de benefícios |
