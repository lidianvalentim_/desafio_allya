package StepDefinitions;

import Page.EntrarEmContatoPage;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class EntrarEmContatoStepDef {
    EntrarEmContatoPage entrarEmContatoPage = new EntrarEmContatoPage();

    @E("Preencho o campo {string}, {string}, {string} e {string}")
    public void preenchoOCampoE(String Nome, String Email, String Telefone, String mensagem) throws Throwable {
        entrarEmContatoPage.inputEntrarEmContato(Nome, Email, Telefone, mensagem);

    }

    @Entao("a mensagem é enviada com sucesso")
    public void aMensagemÉEnviadaComSucesso() throws Throwable {
        entrarEmContatoPage.validarMensagemSucesso();
    }

    @Quando("clico em enviar")
    public void clicoEmEnviar() throws Throwable {
        entrarEmContatoPage.clickBotaoEnviar();
    }

    @Entao("o campo email é exibido com erro")
    public void oCampoEmailÉExibidoComErro() throws Throwable {
        entrarEmContatoPage.validarErroEmail();
    }
}
