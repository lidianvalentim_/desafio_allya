package StepDefinitions;

import Page.HomePage;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

public class HomeStepDef {
    HomePage homePage = new HomePage();

    @Quando("clico no botao Sobre")
    public void clicoNoBotaoSobre() throws Throwable {
        homePage.ValidarPaginaInicial();
        homePage.clicarBotaoSobre();
    }

    @Então("sou redirecionada a secao")
    public void souRedirecionadaASecao() throws Throwable {
        homePage.validarTituloSobre();

    }

    @Quando("clico no botao Contato")
    public void clicoNoBotaoContato() throws Throwable {
        homePage.clicarBotaoContato();
    }


    @Dado("que eu estou na página inicial do site da allya.")
    public void queEuEstouNaPáginaInicialDoSiteDaAllya() throws Throwable {

    }

    @Então("abre uma nova janela")
    public void abreUmaNovaJanela() throws Throwable {
        homePage.novaJanela();
    }

    @Quando("clico no botao Leia Mais")
    public void clicoNoBotaoLeiaMais() {
        homePage.clicarBotaoLeiaMais();

    }
}
