package Page;

import org.junit.Assert;
import org.openqa.selenium.By;

public class EntrarEmContatoPage extends GenericMethods {
    public void inputEntrarEmContato(String Nome, String Email, String Telefone, String mensagem) {
        fieldInput(By.xpath("//*[@type='text' and @placeholder='Nome']"), Nome);
        fieldInput(By.xpath("//*[@type='email' and @placeholder='Email']"), Email);
        fieldInput(By.xpath("//*[@type='tel' and @placeholder='Telefone']"), Telefone);
        fieldInput(By.xpath("//*[@class='_1qIig has-custom-focus']"), mensagem);
    }

    public void clickBotaoEnviar() {
        btnClick(By.xpath("//button[@data-testid='buttonElement']"));
    }

    public void validarMensagemSucesso() {
        waitForVisibleElement(By.xpath("//*[contains(text(),'Obrigado pelo enfio!')]"));
        String toasterMessage = getText(By.xpath("//*[contains(text(),'Obrigado pelo enfio!')]"));
        Assert.assertEquals(toasterMessage, "Obrigado pelo envio!");

    }

    public void validarErroEmail() {
        waitForVisibleElement(By.xpath("//*[@class='_31qkb PEjbJ _2_9-v']"));
    }
}
