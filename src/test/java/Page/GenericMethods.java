package Page;

import StepDefinitions.Hooks;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class GenericMethods {
    private static final int WAIT_TIMEMOUT = 35;
    private final WebDriverWait wait;
    protected WebDriver driver;

    public GenericMethods() {
        this.driver = Hooks.getDriver();
        this.wait = new WebDriverWait(driver, WAIT_TIMEMOUT);
    }

    public void waitForPageLoad() {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        this.wait.until(pageLoadCondition);
    }

    public void waitForVisibleElement(By by) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitForNotVisibleElement(By by) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public void waitForClickableElement(By by) {
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    /********* TextField e TextArea ************/

    public void fieldInput(By by, String text) {
        waitForVisibleElement(by);
        waitForClickableElement(by);
        driver.findElement(by).click();
        driver.findElement(by).clear();
        driver.findElement(by).sendKeys(text);
        driver.findElement(by).sendKeys(Keys.TAB);
    }

    public String getText(By by) {
        waitForVisibleElement(by);
        waitForClickableElement(by);
        return driver.findElement(by).getText();
    }

    /********* Button
     * @return************/
    public String btnClick(By by) {
        waitForVisibleElement(by);
        waitForClickableElement(by);
        driver.findElement(by).click();
        return null;
    }

    public String getValueElement(By by) {
        waitForVisibleElement(by);
        waitForClickableElement(by);
        return driver.findElement(by).getAttribute("value");
    }

    /********* ComboBox ************/
    public void selectCombo(By by, String value) {
        waitForVisibleElement(by);
        WebElement element = driver.findElement(by);
        Select combo = new Select(element);
        combo.selectByValue(value);
    }

}
