package Page;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import java.util.Set;

public class HomePage extends GenericMethods {

    public void ValidarPaginaInicial() {
        waitForVisibleElement(By.xpath("//*[text()='Teste #qallya']"));
    }

    public void clicarBotaoSobre() {
        btnClick(By.id("comp-irys4v5c1label"));
    }

    public void validarTituloSobre() {
        waitForVisibleElement(By.xpath("//*[contains(text(),'o que fazemos?')]"));
        String TituloSobre = getText(By.xpath("//*[contains(text(),'o que fazemos?')]"));
        Assert.assertEquals(TituloSobre, "Afinal, o que fazemos?");
    }

    public void clicarBotaoContato() {
        btnClick(By.id("comp-irys4v5c2label"));
        String TituloContato = getText(By.xpath("//*[@class='font_6' and text()='CONT4TO']"));
        Assert.assertEquals(TituloContato, "CONT4TO");
    }

    public void clicarBotaoLeiaMais() {
        btnClick(By.xpath("//*[@href='https://teste.erro.Renato']"));
    }

    public void novaJanela() {
        Actions newwin = new Actions(driver);
        newwin.keyDown(Keys.SHIFT).keyUp(Keys.SHIFT).build().perform();
        Set<String> windows = driver.getWindowHandles();
        System.out.println(windows);
        for (String window : windows) {
            driver.switchTo().window(window);
        }
    }
}
